# Commenter un événement

## Ajouter un commentaire

Lorsque les commentaires sont autorisés sur un événement, vous pouvez en ajouter en utilisant la section dédiée sur la page&nbsp;:

![image commentaire rose sur un événement](../../images/rose-comment-event-FR.png)  

Vous pouvez aussi répondre à un commentaire spécifique en cliquant sur le lien **Répondre**

![image d'une réponse](../../images/comments-replies-event-FR.png)

## Signaler un commentaire

!!! note
    Vous devez être connecté⋅e à votre compte pour faire une signalement

Pour faire un signalement, vous devez&nbsp;:

  1. cliquer sur l’icône <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M13 14H11V9H13M13 18H11V16H13M1 21H23L12 2L1 21Z" />
    </svg> visible en survolant le commentaire&nbsp;:
      ![image icône signalement](../../images/report-comment-icon-FR.png)
  * [Optionnel mais recommandé] donner la raison du signalement
      ![modale de signalement](../../images/comment-report-FR.png)
  * cliquer sur le bouton **Envoyer le signalement**.

## Supprimer votre commentaire

Pour supprimer un commentaire vous devez cliquer sur l'icône <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="currentColor" d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" /></svg> visible en survolant le commentaire.

![suppression d'un commentaire](../../images/delete-comment-FR.png)
