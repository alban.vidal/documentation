# Gérer les rôles

## Roles

| ACTIONS                                | ADMINISTRATEUR⋅RICE   | MODÉRATEUR⋅ICE       | MEMBRE          |
| :--------------------------------------|:---------------:|:---------------:|:---------------:|
| Commencer / modifier / supprimer une discussion     | ✔               | ✔              | ✔ *             |
| Ajouter / modifier / supprimer une ressource         | ✔               | ✔               | ✔               |
| Créer / supprimer un événement               | ✔               | ✔               | ❌              |
| Créer / modifier / supprimer un message public  | ✔               | ✔               | ❌              |
| Ajouter des membres                            | ✔               | ❌              | ❌              |
| Gérer les rôles                           | ✔               | ❌              | ❌              |
|                                        |                 |                 |                 |

\* ayant créé la discussion

![](../../images/group-members-list-FR.png)

!!! note
    Par défaut les comptes invités ont le rôle de **membre**.

## Promouvoir

Vous pouvez promouvoir un compte vers **moderateur·ice** ou **Administrateur·ice**. Pour ce faire, vous devez&nbsp;:

  1. cliquer sur le bouton **Mes groupes** dans la barre de menu supérieure
  * cliquer sur le groupe que vous souhaitez gérer
  * cliquer sur le lien **Ajouter / Supprimer...** dans l'image d'illustration de votre groupe
  * cliquer sur le bouton **Promouvoir** devant l'utilisateur que vous souhaitez promouvoir.

!!! info
    Vous ne pouvez promouvoir que d'un échelon à la fois.

## Rétrograder

Vous pouvez rétrograder un compte vers **moderateur·ice** ou **Administrateur·ice**. Pour ce faire, vous devez&nbsp;:

  1. cliquer sur le bouton **Mes groupes** dans la barre de menu supérieure
  * cliquer sur le groupe que vous souhaitez gérer
  * cliquer sur le lien **Ajouter / Supprimer...** dans l'image d'illustration de votre groupe
  * cliquer sur le bouton **Rétrograder** devant l'utilisateur que vous souhaitez promouvoir.

!!! info
    Vous ne pouvez rétrograder que d'un échelon à la fois.

## Exclure

Vous pouvez exclure un compte du groupe. Pour ce faire, vous devez&nbsp;:

  1. cliquer sur le bouton **Mes groupes** dans la barre de menu supérieure
  * cliquer sur le groupe que vous souhaitez gérer
  * cliquer sur le lien **Ajouter / Supprimer...** dans l'image d'illustration de votre groupe
  * cliquer sur le bouton **Exclure** en face du compte

!!! note
    Si le compte à exclure est modérateur ou admin, vous devez commencer par le [rétrograder](#retrograder).
